# -*- coding: utf-8 -*-
import json
import requests
import math        


"""
fontion qui renvoi une liste de points d'intérêts dans une bbox
renvoie seulement les points avec au moins un tags
@param {Liste} bbox : [xmin,ymin,xmax,ymax]
@return {Liste}
"""
def recherche_points_interet_dans_bbox(bbox):
    xmin,ymin,xmax,ymax = bbox
    
    dataRequest  = "<osm-script output=\"json\">"
    dataRequest += "<union>"
    dataRequest += "<query type=\"node\">"
    dataRequest += "<bbox-query e=\"" + str(xmax) + "\" n=\"" + str(ymax) + "\" s=\"" + str(ymin) + "\" w=\"" + str(xmin) + "\" />"
    
    # ajout d'un filtre pour ne récuperrer que les point avec des "tags"
    dataRequest += "<filter>"
    dataRequest += "<eval-greater>"
    dataRequest += "<eval-prop-count type=\"tags\"/>"
    dataRequest += "<eval-fixed v=\"0\"/>"
    dataRequest += "</eval-greater>"
    dataRequest += "</filter>"
    dataRequest += "</query>"
    dataRequest += "</union>"
    dataRequest += "<print mode=\"meta\"/>"
    dataRequest += "</osm-script>"

    
    # URL_OVERPASS_API = "http://overpass-api.de/api/interpreter";
    # URL_OVERPASS_API = "https://z.overpass-api.de/api/interpreter";
    URL_API = 'https://lz4.overpass-api.de/api/interpreter'  # data=[out:json]   
    # proxies = {
    #    'http': 'http://10.0.4.2:3128',
    #    'https': 'http://10.0.4.2:3128',
    # }  
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    # response = requests.post(URL_API, data=dataRequest, headers=headers, proxies=proxies)
    response = requests.post(URL_API, data=dataRequest, headers=headers)
    http_status = response.status_code
    
    # Si la requête s'est bien passée:
    if http_status == 200:
        data = response.content
        obj = json.loads(data)
     
    
    return obj["elements"]


"""
Calcule la distance entre 2 points en mètre
@param {Tuples} p1 : point
@param {Tuples} p1 : point
@return {Float} : distance en mètre
"""
def calcule_distance(p1,p2):
    lat1 = p1[1]
    lon1 = p1[0]
    
    lat2 = p2[1]
    lon2 = p2[0]
    
    if (lat1 == lat2) and (lon1 == lon2):
        return 0
    else:
        theta = lon1-lon2
        dist = math.sin(math.radians(lat1)) * math.sin(math.radians(lat2)) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.cos(math.radians(theta))
        dist = math.acos(dist)
        dist = math.degrees(dist)
        
        miles = dist * 60 * 1.1515
        metre = miles * 1609.344

        return (metre)


"""
Renvoie les points d'intérêt au alentour d'un point en fonction d'un rayon en mètre
@param {Tuples} p_origine : point
@param {Dict} list_pi : dictionaire des points d'interet
@param {Integer} distance : rayon en mètre
@return {Dict}
"""    
def trouve_point_alentour(p_origine, list_pi, distance):
    points_alentour = []
    for point_pi in list_pi:
        dist = calcule_distance(p_origine, (point_pi["lon"],point_pi["lat"]))
        if dist<=distance:
            points_alentour.append(point_pi)
    return points_alentour

  
"""
renvoi une liste de poids associé à un point d'intérêt  en fonction de ces tags
plus le poids est petit plus le point d'intérêt  est important
@param {List} list_point : liste des points d'interet
@return {List}
""" 
def get_list_poids(list_point):
    poids_point = []
    for point in list_point:
        
        poids = 10
        if "amenity" in point["tags"]:
            if point["tags"]["amenity"] == "place_of_worship":
                poids =94
            if point["tags"]["amenity"] == "fountain":
                poids =91
            if point["tags"]["amenity"] == "police" or point["tags"]["amenity"] == "fire_station":
                poids =90
            if point["tags"]["amenity"] == "cinema":
                poids =86
            if point["tags"]["amenity"] == "theatre":
                poids =85
            if point["tags"]["amenity"] == "restaurant" or point["tags"]["amenity"] == "cafe" or point["tags"]["amenity"] == "pub" or point["tags"]["amenity"] == "bar":
                poids =84
            if point["tags"]["amenity"] == "post_office":
                poids =83
            if point["tags"]["amenity"] == "pharmacy":
                poids =79
            if point["tags"]["amenity"] == "school" or point["tags"]["amenity"] == "university":
                poids =75
            if point["tags"]["amenity"] == "hospital":
                poids =75
            if point["tags"]["amenity"] == "fuel":
                poids =75
            if point["tags"]["amenity"] == "library":
                poids =71
            if point["tags"]["amenity"] == "embassy":
                poids =45
            if point["tags"]["amenity"] == "bank":
                poids =38
            if point["tags"]["amenity"] == "bicycle_rental" or point["tags"]["amenity"] == "bus_station":
                poids =30
            if point["tags"]["amenity"] == "post_box" or point["tags"]["amenity"] == "toilets" or point["tags"]["amenity"] == "telephone":
                poids =20
       
        if "building" in point["tags"]:
            if point["tags"]["building"] == "church" or point["tags"]["building"] == "mosque" or point["tags"]["building"] == "synagogue" or point["tags"]["building"] == "cathedral":
                poids =94
            if point["tags"]["building"] == "school" or point["tags"]["building"] == "university":
                poids =75
          
        if "historic" in point["tags"]:
            if point["tags"]["historic"] == "memorial" or point["tags"]["historic"] == "attraction":
                poids =91
                
        if "tourism" in point["tags"]:
            if point["tags"]["tourism"] == "hotel":
                poids =84
            if point["tags"]["tourism"] == "museum":
                poids =79
                
        if "railway" in point["tags"]:
           if point["tags"]["railway"] == "subway_entrance":
               poids =83
               
        if "shop" in point["tags"]:
            if point["tags"]["shop"] == "supermarket":
                poids =81
            if point["tags"]["shop"] == "bakery" or point["tags"]["shop"] == "chemist" or point["tags"]["shop"] == "clothes" or point["tags"]["shop"] == "jewelry" or point["tags"]["shop"] == "kiosk":
                poids =50
            if point["tags"]["shop"] == "hairdresser":
                poids =42
                
        if "leisure" in point["tags"]:
            if point["tags"]["leisure"] == "park" or point["tags"]["leisure"] == "playground":
                poids =75
                
        if "public_transport" in point["tags"]:
            if point["tags"]["public_transport"] == "platform" or point["tags"]["public_transport"] == "stop_position":
                poids =30
        
        poids_point.append(poids)
    return poids_point


"""
Calcule l'équation d'une droite entre 2 points
@param {Tuples} p1
@param {Tuples} p2
@return {Tuples}
""" 
def calc_equation_droite(p1,p2):
    try : 
        a = (p1[1] - p2[1]) / (p1[0] - p2[0])
        b = p1[1] - a * p1[0]
    except ZeroDivisionError: 
        (a,b) = (-1,-1)
    return (a,b)


"""
Calcule l'équation d'une droite perpendiculaire  à une autre droite
et passant par un point
@param {Tuples} d : droite
@param {Tuples} p : point
@return {Tuples} : équation de la droite perpendiculaire 
""" 
def droite_perpendiculaire(d, p):
    a,b = d
    x,y = p
    a_perp = -(1/a)
    b_perp = y-(a_perp*x)  
    return (a_perp,b_perp)


"""
Retour de quel côté  se trouve un point par rapport à une droite entre 2 points
@param {Tuples} p1
@param {Tuples} p2
@param {Tuples} p3 : point à testé
@return {Tuples} : équation de la droite perpendiculaire
""" 
def calc_cote(p1,p2,p3):
    x_a,y_a = p1
    x_b,y_b = p2
    x_c,y_c = p3
    cote = (x_b-x_a)*(y_c-y_a)-(y_b-y_a)*(x_c-x_a)
    if cote > 0:
        return "gauche"
    elif cote < 0:
        return "droite"
    else:
        return "aligne"


"""
Retourne si un point d'intérêt est du même coté que le prochain point du parcours
@param {Tuples} point_decision
@param {Tuples} next_point_decision
@param {Tuples} landmark
@param {Boolean} last : True si c'est le dernier point du parcour
@return {Interger} : 1 si du même coté, 0 sinon
""" 
def get_side(point_decision, next_point_decision, landmark, last):
    d = calc_equation_droite(point_decision, next_point_decision)
    d_perp = droite_perpendiculaire(d, point_decision)
    a,b = d_perp
    
    y=a*(point_decision[0]+1)+b
    
    p1 = (point_decision[0],point_decision[1])
    p2 = (point_decision[0]+1,y)
    cote_next = calc_cote(p1,p2,next_point_decision)
    cote_landmark = calc_cote(p1,p2,landmark)
    if cote_landmark==cote_next:
        if last:
            return 0
        else:
            return 1
    else:
        if last:
            return 1
        else:
            return 0
    
"""
Retourne si un point d'intérêt est derrière ou devant un point du parcours
par rapport au sens du parcours
@param {Tuples} point_decision
@param {Tuples} last_point_decision
@param {Tuples} landmark
@param {Boolean} first : True si c'est le premier point du parcour
@return {Interger} : 1 si devant, 0 si deriere
"""
def get_location(point_decision, last_point_decision, landmark, first):
    d = calc_equation_droite(point_decision, last_point_decision)
    d_perp = droite_perpendiculaire(d, point_decision)
    a,b = d_perp
    
    y=a*(point_decision[0]+1)+b
    
    p1 = (point_decision[0],point_decision[1])
    p2 = (point_decision[0]+1,y)
    cote_last = calc_cote(p1,p2,last_point_decision)
    cote_landmark = calc_cote(p1,p2,landmark)
    if cote_landmark==cote_last:
        # le point est derriere
        if first:
            return 1
        else:
            return 0
    else:
        if first:
            return 0
        else:
            return 1

"""
Retourne la liste des poids d'une liste de points d'intérêt
par rapport à un point du parcours
@param {Tuples} p_dec
@param {Tuples} p_next : prochain point du parcour
@param {Tuples} p_last : point précédent du parcour
@param {Liste} list_landmark : liste des points d'interet
@return {Liste} : liste des poids
"""
def calc_poids(p_dec, p_next, p_last, list_landmark):
    poids = get_list_poids(list_landmark)
    dmax = 100
    cmax = 94
    for i in range(len(list_landmark)):
        d = calcule_distance(p_dec,(list_landmark[i]["lon"],list_landmark[i]["lat"]))
        c = poids[i]
        
        if p_next == None:
            s = get_side(p_dec, p_last, (list_landmark[i]["lon"],list_landmark[i]["lat"]),True)
        else:
            s = get_side(p_dec, p_next, (list_landmark[i]["lon"],list_landmark[i]["lat"]),False)
        
        if p_last == None:
            l = get_location(p_dec, p_next, (list_landmark[i]["lon"],list_landmark[i]["lat"]),True)
        else:
            l = get_location(p_dec, p_last, (list_landmark[i]["lon"],list_landmark[i]["lat"]),False)
        # v --> visibility
        v=1
        
        wd=1.2
        wc=1.2
        ws=1
        wl=1
        
        poids_complex = v*((dmax-d)*wd-(cmax-c)*(dmax/cmax)*wc+s*ws+l*wl)
        poids[i]=poids_complex
    
    return poids

    
"""
renvoie le poids minimum d'une liste de poids
@param {List} list_poids : liste des poids
@return {List}
"""
def get_poids_max(list_poids):
    poids_max = -100000
    imax = 0
    for i in range(len(list_poids)):
        if list_poids[i] > poids_max:
            poids_max = list_poids[i]
            imax = i
    return poids_max,imax


"""
fonction qui renvoie la liste de tout les point d'intérêt que l'ont vas utiliser pour guider l'utilisateur
@param {List} points_parcour : liste des points du parcours
@param {List} points_interet : liste des points d'interet (tous les points dans la bbox)
@return {List}
"""
def get_ensemble_points_pertinent(points_parcour, points_interet):
    list_points_interet = []
    # parcour tous les point crusio
    for i in range(0,len(points_parcour)):
        # cherche les point dans un rayon
        points_alentour = trouve_point_alentour(points_parcour[i], points_interet, 100)
        if i==0:
            poids = calc_poids(points_parcour[i], points_parcour[i+1], None, points_alentour)
        elif i==len(points_parcour)-1:
            poids = calc_poids(points_parcour[i], None, points_parcour[i-1], points_alentour)
        else:
            poids = calc_poids(points_parcour[i], points_parcour[i+1], points_parcour[i-1], points_alentour)
        # poids_max,imax = get_poids_max(poids)
        # list_points_interet.append(points_alentour[imax])
            
        print(i)
        ajout = False
        while not ajout:
            poids_max,imax = get_poids_max(poids)
            if points_alentour[imax] not in list_points_interet:
                list_points_interet.append(points_alentour[imax])
                ajout = True
            else:
                poids[imax] = -100000
    return list_points_interet
    

"""
Fonction qui lit permet de lire le fichier json contenant les infos liées à la raquette osm
@param {String} filename : chemin vers le fichier
@return {Liste} : liste de dictionnaire 
"""
def read_json_file(filename):
    with open(filename, 'r') as openfile:
 
        # Reading from json file
        json_object = json.load(openfile)
     
    return json_object



"""
Fonction qui permet d'avoir le type et nom de chaque point d'une liste de points d'intérêt
@param {Liste} list_point : liste des points d'interet
@return {Liste} : liste de dictionnaire 
"""
def get_info_point(list_point):
    infos = []
    for point in list_point:
        infos.append({})
        if "amenity" in point["tags"]:
            infos[-1]["type"] = point["tags"]["amenity"]         
       
        if "building" in point["tags"]:
            infos[-1]["type"] = point["tags"]["building"]
          
        if "historic" in point["tags"]:
            infos[-1]["type"] = point["tags"]["historic"]
                
        if "tourism" in point["tags"]:
            infos[-1]["type"] = point["tags"]["tourism"]
                
        if "railway" in point["tags"]:
          infos[-1]["type"] = point["tags"]["railway"]
               
        if "shop" in point["tags"]:
            infos[-1]["type"] = point["tags"]["shop"]
                
        if "leisure" in point["tags"]:
            infos[-1]["type"] = point["tags"]["leisure"]
                
        if "public_transport" in point["tags"]:
            infos[-1]["type"] = point["tags"]["public_transport"]
            
        if "name" in point["tags"]:
          infos[-1]["name"] = point["tags"]["name"]
        
    return infos