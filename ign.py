import json
import requests
import copy


# Lien requete IGN
# https://wxs.ign.fr/calcul/geoportail/itineraire/rest/1.0.0/route?resource=bdtopo-pgr&profile=pedestrian&optimization=fastest&start=2.335860,48.861158&end=2.312605,48.856743&intermediates=&constraints={%22constraintType%22:%20%22banned%22,%22key%22:%22wayType%22,%22operator%22:%22=%22,%22value%22:%22tunnel%22}&geometryFormat=geojson&crs=EPSG:4326&getSteps=true&getBbox=true&waysAttributes=nature


"""------------------------------------------------------------------------
Fonction requete_itineraire - récupére un itineraire auprès du service de calcul d'itinéraire de l'IGN

@param {Float} Point_depart: Point de départ du itinéraire
@param {Float} Point_arrive: Point d'arrivée du itinéraire
@return {text} page: la page en format text  
@return {int} page_status: le "status code" de la requête

------------------------------------------------------------------------"""


def requete_itineraire(Point_depart, Point_arrivee):
    URL_IGN_ITINERAIRE = (
        "https://wxs.ign.fr/calcul/geoportail/itineraire/rest/1.0.0/route?"  # URL
    )
    URL_IGN_ITINERAIRE += "resource=bdtopo-pgr&profile=pedestrian&optimization=fastest"
    URL_IGN_ITINERAIRE += (
        "&start="
        + str(Point_depart[0])
        + ","
        + str(Point_depart[1])
        + "&end="
        + str(Point_arrivee[0])
        + ","
        + str(Point_arrivee[1])
    )
    URL_IGN_ITINERAIRE += '&intermediates=&constraints={"constraintType":"banned","key":"wayType","operator":"=","value":"tunnel"}'
    URL_IGN_ITINERAIRE += "&geometryFormat=geojson&crs=EPSG:4326&getSteps=true&getBbox=true&waysAttributes=nature"
    page = requests.get(URL_IGN_ITINERAIRE)
    page_status = page.status_code
    return page, page_status


"""------------------------------------------------------------------------
Fonc coordonnees_itineraire: récupère les coordonnées et le "bounding box"
de la requête précédente


@param {Float} Point_depart: Point de départ du itinéraire
@param {Float} Point_arrive: Point d'arrivée du itiéeraire
@return {liste} L_coords: une liste de coordonnées qui forme l'itinéraire  
@return {liste} bbox: le "status code" de la requête
------------------------------------------------------------------------"""


def coordonnees_itineraire(Point_depart, Point_arrivee):
    page, page_status = requete_itineraire(Point_depart, Point_arrivee)
    if page_status == 200:
        data = page.text
        obj = json.loads(data)
        steps = obj["portions"][0]["steps"]
        bbox = obj["bbox"]
        L_coords = []
        L_bbox = []
        for step in steps:
            geom = step["geometry"]
            coords = geom["coordinates"]
            L_coords.append(coords)
        for point in bbox:
            L_bbox.append(point)

    return L_coords, L_bbox


"""------------------------------------------------------------------------
Fonc coordonnees_itineraire_fichier: on recupére les coordonnées et le "bounding box"
d'un fichier .json obtenu dans la requete itinéraire


@param {Float} Point_depart: Point de départ du itinéraire
@param {Float} Point_arrive: Point d'arrivée du itinéraire
@return {liste} L_coords: une liste de coordonnées qui forme l'itinéraire  
@return {liste} bbox: le "status code" de la requete
------------------------------------------------------------------------"""


def coordonnees_itineraire_fichier(filename):

    f = open(filename)
    obj = json.load(f)
    steps = obj["portions"][0]["steps"]
    bbox = obj["bbox"]
    L_coords = []
    L_bbox = []
    for step in steps:
        geom = step["geometry"]
        coords = geom["coordinates"]

        L_coords.append(coords)
    for point in bbox:
        L_bbox.append(point)

    return L_coords, L_bbox


"""------------------------------------------------------------------------
Fonction agrandir_bbox 

@param {liste} bbox: liste avec 4 coordonnées (x,y)
@return {liste} grandbox: liste avec 4 coordonnées (x,y) agrandie de 100 metres de chaque côté
------------------------------------------------------------------------"""


def agrandir_bbox(bbox):
    grand_bbox = copy.deepcopy(bbox)
    grand_bbox[0] = grand_bbox[0] - 0.001
    grand_bbox[1] = grand_bbox[1] - 0.001
    grand_bbox[2] = grand_bbox[2] + 0.001
    grand_bbox[3] = grand_bbox[3] + 0.001
    return grand_bbox


"""------------------------------------------------------------------------
Fonction liste_simplifiee  - récupère une liste issue de la requête IGN
et la simplifie en enlevant les points entre le premier et le dernier point de chaque étape du trajet et la transformant
en une liste de dimension 1xn

@param {liste} liste_de_listes: liste des étapes composées de coordonnées formant l'itineraire
@return {liste} liste_dimension_1: liste des coordonées formant l'itinéraire
------------------------------------------------------------------------"""


def liste_simplifiee(liste_itineraire_ign):
    liste__premier_dernier_element = []
    for element in liste_itineraire_ign:
        if len(element) > 1:
            liste__premier_dernier_element.append([element[0], element[-1]])
        else:
            liste__premier_dernier_element.append(element)

    liste_dimension_1 = []
    for element in liste__premier_dernier_element:
        for point in element:
            liste_dimension_1.append(point)
    return liste_dimension_1
