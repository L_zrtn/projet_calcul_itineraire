import math

"""----------Fonction calcule_distance--------------
Calcule la distance entre 2 points (long, lat) en metre

@param {Tuples} p1 : point
@param {Tuples} p2 : point
@return {Float} : distance en metre
-------------------------------------------------"""


def calcule_distance(p1, p2):
    lat1 = p1[1]
    lon1 = p1[0]

    lat2 = p2[1]
    lon2 = p2[0]

    if (lat1 == lat2) and (lon1 == lon2):
        return 0
    else:
        theta = lon1 - lon2
        dist = math.sin(math.radians(lat1)) * math.sin(math.radians(lat2)) + math.cos(
            math.radians(lat1)
        ) * math.cos(math.radians(lat2)) * math.cos(math.radians(theta))
        dist = math.acos(dist)
        dist = math.degrees(dist)

        miles = dist * 60 * 1.1515
        metre = miles * 1609.344

        return metre


"""------------Fonction calcul_angle-----------------------

Calcule l'angle entre 2 points en degrés
@param {Tuples} p1,p2,p3 : point de coordonnées (x,y)
@return {Float} : angle en degrés

la fonc atan2  prend en radians l'angle entre la partie positive de l'axe des abscisses
d'un plan et le point de ce plan de coordonnées (x, y).
On utilise le difference des arctans pour trouver l'angle entre 2 segments
-------------------------------------------------"""


def calcul_angle(a, b, c):
    angle = math.degrees(
        math.atan2(c[1] - b[1], c[0] - b[0]) - math.atan2(a[1] - b[1], a[0] - b[0])
    )
    if angle < 0:
        angle = angle + 360
    return angle


""" ---------Fonction tourner_droite_ou_gauche---------

A partir d'un angle renvoit la direction relative à suivre 
@param {float}: float, 0 =< angle <= 360
@return {string} : direction à suivre

-------------------------------------------------"""


def tourner_droite_ou_gauche(angle):  ######################!!!!!!!!!!!!
    ######################j'ai change les parametres des angles!!!!!!!!!!
    if angle < 35 or angle >= 325:
        direction = "Continuer tout droit"
    elif angle >= 35 and angle <= 145:
        direction = "Tourner à droite"
    elif angle >= 145 and angle <= 215:
        direction = "Continuer tout droit"
    elif angle >= 215 and angle < 325:
        direction = "Tourner à gauche"

    return direction


""" ---------Fonction avant_ou_apres---------

@param {float}:  0 =< angle <= 360
@return {string} : avant ou apres

"""


def avant_ou_apres(angle):

    if angle <= 90 or angle > 270:
        position = "après"
    elif angle > 90 and angle <= 270:
        position = "avant"

    return position


""" ---------Fonction situe_droite_ou_gauche ---------

@param {float}: float, 0 =< angle <= 360
@return {string} : droite ou gauche

"""


def situe_droite_ou_gauche(angle):
    if angle >= 180:
        cote = "droite"
    else:
        cote = "gauche"
    return cote


