import ign
import geometry
import export_shp
import osm


Point_depart = [2.335860, 48.861158]  # Coordonnées WGS84 de la Pyramide du Louvre
Point_arrivee = [2.312605, 48.856743]  # Coordonnées WGS84 de l'hotel des Invalides


troncon_itineraire, bbox = ign.coordonnees_itineraire_fichier(
    "./resultat_ign.json"
)  # on charge un fichier avec la requete
# c'est possible aussi de utilier les fonctions requete_itineraire et coordonnees_itineraire pour faire la requete a l'IGN en passant par internet

liste_itineraire_simplifie = ign.liste_simplifiee(
    troncon_itineraire
)  # on simplifie la requete IGN

grand_bbox = ign.agrandir_bbox(
    bbox
)  # on agrandie le bbox pour ne pas couper d'infos au bord de l'itineraire

# points_interet = osm.recherche_points_interet_dans_bbox(grand_bbox)  # Recherche des points d'interet sur OSM/FICHIER
points_interet = osm.read_json_file("points_interet.json")



liste_points_pertinents = osm.get_ensemble_points_pertinent(
    liste_itineraire_simplifie, points_interet
)  # on prends tous les points pertinents pour chaque point de l'itineraire


"""-----------------------------------------------
la  fonction_finale prend en entrée un itineraire et renvoie une liste d'instructions pour
chaque point stratégique

@param {liste} itineraire: liste avec liste de points obtenus avec la requete IGN
@param {liste} liste_de_poi: Une liste avec tous les POIs déjà calculés auparavant

@return {liste} instructions_finales: liste avec les instructions pour le piéton

"""


def fonction_finale(itineraire, liste_de_poi):

    points_pertinents_choisis = []  # on cree une liste vide pour exporter un shapefile

    instructions_finales = []

    etape = 0

    info_liste_points_pertinents = osm.get_info_point(liste_de_poi)

    for i in range(0, len(itineraire) - 1, 1):

        """
        On commence à la 2eme etape qui represente le noeud d'index 1

        pour calculer l'angle on utilise :
            le noeud anterieur comme P1
            le noeud present comme p2
            le noeud future comme p32
        """
        noeud_anterieur = itineraire[i - 1]
        noeud_present = itineraire[i]
        noeud_futur = itineraire[i + 1]

        angle_itineraire = geometry.calcul_angle(
            noeud_anterieur, noeud_present, noeud_futur
        )

        instruction_itineraire = geometry.tourner_droite_ou_gauche(angle_itineraire)

        if instruction_itineraire == "Continuer tout droit" and (
            geometry.calcule_distance(noeud_present, noeud_futur) < 15
            or geometry.calcule_distance(noeud_anterieur, noeud_present) < 15
        ):
            continue  # si on enleve le point on etapese pour le prochaine iteration du loop
        etape = etape + 1

        """
        après on calcule l'orientation du point d'interet choisi
        """
        angle_poi = geometry.calcul_angle(
            noeud_anterieur,
            noeud_present,
            [
                liste_de_poi[i]["lon"],
                liste_de_poi[i]["lat"],
            ],
        )

        if instruction_itineraire == "Continuer tout droit":
            orientation_poi = " en passant à côté "
        else:
            orientation_poi = geometry.avant_ou_apres(angle_poi)
        # finalement on append les points a la solution finale
        if i == 0:

            loc_poi = geometry.situe_droite_ou_gauche(angle_poi)
            instructions_finales.append(
                [
                    etape,
                    "dirigez-vous vers",
                    liste_de_poi[i + 1]["tags"],
                    "avec",
                    liste_de_poi[i]["tags"]["name"],
                    " à votre ",
                    loc_poi,
                ]
            )

        else:
            if "name" in liste_de_poi[i]["tags"]:
                instructions_finales.append(
                    [
                        etape,
                        instruction_itineraire,
                        orientation_poi,
                        info_liste_points_pertinents[i]["type"],
                        liste_de_poi[i]["tags"]["name"],
                    ]
                )

            else:
                instructions_finales.append(
                    [
                        etape,
                        instruction_itineraire,
                        orientation_poi,
                        liste_de_poi[i]["tags"],
                    ]
                )

        points_pertinents_choisis.append(
                [noeud_present, liste_de_poi[i]]
            )  # pour exporter un fichier shapefile
    """
    # on exporte un fichier shapefile avec le POIs choisi lié au point de l'itinéraire
    for i in range(len(points_pertinents_choisis)):
        segment = (
            points_pertinents_choisis[i][0],
            [
                points_pertinents_choisis[i][1]["lon"],
                points_pertinents_choisis[i][1]["lat"],
            ],
        )
        export_shp.polyligne_file(
            "./shapefile/points_choisis/" + "point_choisi_" + str(i), segment
        )
    """
    # on renvoi le resultat
    return instructions_finales


itineraire_finale = fonction_finale(liste_itineraire_simplifie, liste_points_pertinents)
print(itineraire_finale)


# on sauvegarde le resultat dans le format txt

with open("instructions.txt", "w") as filename:
    for instruction in itineraire_finale:
        # write each item on a new line
        filename.write("%s\n" % instruction)
    print("fin")

################################################################### extras


"""-----------------------------Création du shapefile de l'itineraire complet
point_itineraire = []
for troncon in troncon_itineraire:
    for point in troncon:
        point_itineraire.append(point)
export_shp.polyligne_file("./shapefile/itineraire_complet", point_itineraire)

------------------------------------------------------------------------"""


"""-----------------------------Création du shapefile de la bbox de l'itineraire
xmin, ymin, xmax, ymax = bbox
point_bbox = [(xmin, ymin), (xmin, ymax), (xmax, ymin), (xmax, ymax)]
export_shp.point_file("./shapefile/bbox_itineraire", point_bbox)
------------------------------------------------------------------------"""


"""-----------------------------Création du shapefile de la bbox agrandie de l'itineraire
xmin, ymin, xmax, ymax = grand_bbox
point_bbox = [(xmin, ymin), (xmin, ymax), (xmax, ymin), (xmax, ymax)]
export_shp.point_file("./shapefile/bbox_itineraire_agrandie", point_bbox)
"""

"""-----------------------------Création du shapefile des points d'interet
liste_points_interet = []
for point in points_interet:
    liste_points_interet.append((point["lon"], point["lat"]))
export_shp.point_file("./shapefile/points_interet", liste_points_interet)
------------------------------------------------------------------------"""
