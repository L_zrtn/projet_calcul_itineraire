# Projet calcul itinéraire

Projet réalisé dans le cadre du Master 1 géomatique de l'Université Gustave Eiffel - ENSG
## Description

Le but de ce projet est d'obtenir les instructions de navigation d'un itinéraire piéton en utilisant des points de repères différents de ceux utilisés pour la navigation en voiture.

## Groupe

- Lisa Zaratin
- Romain Couret
- Thiago Carmuega-Rabacal

## Execution du code
Placer tous les fichiers .py ainsi que les fichiers .json dans un même répertoire.

Pour obtenir les instructions de navigation, executer le fichier main.py 

## Biblio 
 https://agile-online.org/images/conferences/2017/documents/shortpapers/46_ShortPaper_in_PDF.pdf

 https://www.mdpi.com/2220-9964/6/3/64/htm
 
