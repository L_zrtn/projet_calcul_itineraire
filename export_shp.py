import shapefile


"""------------------------------------------------------------------------
Plusieurs fonctions pour sauvegarder des fichiers en format shapefile

-points 
-polyligne
-polygon

@param nom_fichier{str}: chemin/nom du fichier
@param list_point{list}: liste de coordonnées

------------------------------------------------------------------------"""


def polyligne_file(nom_fichier, list_point):

    path = nom_fichier
    with shapefile.Writer(path, shapefile.POLYLINE) as shp:
        shp.field("name", "C")
        shp.line([list_point])
        shp.record("line1")
    shp.close()


def polyligne_file_v2(nom_fichier, list_point):
    path = nom_fichier
    with shapefile.Writer(path, shapefile.POLYLINE) as shp:
        shp.field("name", "C")

        for l in list_point:

            shp.line([l])
            shp.record("line1")


def point_file(nom_fichier, list_point):
    path = nom_fichier
    with shapefile.Writer(path, shapefile.POINT) as shp:
        shp.field("name", "C")

        for point in list_point:

            shp.point(point[0], point[1])
            shp.record("point")


def polygon_file(nom_fichier, polygon):
    path = nom_fichier
    with shapefile.Writer(path, shapefile.POLYGON) as shp:
        shp.field("name", "C")

        for i in range(len(polygon)):

            shp.poly([polygon[i]])
            shp.record(str(i) + " polygon")
